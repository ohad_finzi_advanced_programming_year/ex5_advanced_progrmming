//#include "Menu.h"

#include "Point.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"

//TEST MEMORY LEAK
#define _CRTDBG_MAP_ALLOC 
#include <crtdbg.h> 


void checkPoint();
void checkCircle();
void checkTriangle();
void checkRecatangle();
void checkArrow();


void main()
{	
	//checkPoint();

	//checkCircle();

	//checkTriangle();

	//checkRecatangle();

	checkArrow();

	printf("Leaks: %d", _CrtDumpMemoryLeaks());
}


void checkPoint()
{
	Point p1(100, 100);
	std::cout << p1.getX() << p1.getY() << std::endl;  //should be "100 100"

	Point p2(p1);
	std::cout << p2.getX() << p2.getY() << std::endl;  //should be "100 100"

	Point p3 = p1 + p2;
	std::cout << p3.getX() << p3.getY() << std::endl;  //should be "200 200"

	p3 += p2;
	std::cout << p3.getX() << p3.getY() << std::endl;  //should be "300 300"

	std::cout << p1.distance(p3) << std::endl; //should be "282.843"
}


void checkCircle()
{
	try
	{
		Point p1(100, 100);

		Circle c1(p1, 50, "Circle", "c1");
		std::cout << c1.getRadius() << "\n" << c1.getArea() << "\n" << c1.getPerimeter() << std::endl;  //should be 50 - 314 - 7853

		Point p2(c1.getCenter());
		std::cout << p2.getX() << p2.getY() << std::endl;  //should be "100 100"

		Circle c2(p1, -1, "Circle", "c2");
		//should trigger the exception because of the negative radius input
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

}


void checkTriangle()
{
	try
	{
		Point p1(10, 10);
		Point p2(10, 20);
		Point p3(20, 20);

		Triangle t1(p1, p2, p3, "Triangle", "t1");
		std::cout << t1.getPerimeter() << "\n" << t1.getArea() << std::endl;  //should be 34.12 - 50

		Triangle t2(p1, p1, p1, "Triangle", "t2");
		//should trigger the exception because the triangle is on a line
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}
}


void checkRecatangle()
{
	try
	{
		Point p1(10, 20);

		myShapes::Rectangle r1(p1, 20, 10, "Recatangle", "r1");
		std::cout << r1.getPerimeter() << "\n" << r1.getArea() << std::endl;  //should be 60 - 200

		myShapes::Rectangle r2(p1, -1, -1, "Rectangle", "r2");
		//should trigger the exception because the triangle is on a line
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}
}


void checkArrow()
{
	Point p1(10, 10);
	Point p2(50, 10);

	Arrow a1(p1, p2, "Arrow", "a1");
	std::cout << a1.getPerimeter() << "\n" << a1.getArea() << std::endl;  //should be 40 - 0
}

