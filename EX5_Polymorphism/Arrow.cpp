#include "Arrow.h"


/*
The function constructs the current Arrow class according to the givne elemetns
Input: two points of the arrow, the type (Arrow) and name of object
*/
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name):
	Shape(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
}


/*
The functions deconstructs the allocated dynamcic memory allocated for the current Arrow class
*/
Arrow::~Arrow()
{
	//nothing to delete here
}


/*
The function draws the current Arrow on the given canvas
Input: the canvas to draw on
*/
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}


/*
The function clears the draw of the current Arrow on the given canvas
Input: the canvas to draw on
*/
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


/*
The functions reutrns the area of the current Arrow class
Ouput: returns 0 (each arrow's area is 0 in this program)
*/
double Arrow::getArea() const
{
	return 0.0;
}


/*
The funtion returns the perimeter of the current Arrow class
Output: returns the distance between the two points of the arrow (which is the perimeter of each arrow in this program)
*/
double Arrow::getPerimeter() const
{
	return _points[0].distance(_points[1]);
}


/*
The function moves the 2 points of the current arrow according to the given point
Input: a Point class
*/
void Arrow::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
}

