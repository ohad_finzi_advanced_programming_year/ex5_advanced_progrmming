#include "Polygon.h"


/*
The function constructs the current Polygon class according to the given elemetns
Input: the type of the polygon and the name of it
*/
Polygon::Polygon(const std::string& type, const std::string& name):
	Shape(type, name)
{
}


/*
The functions deconstructs the allocated dynamic memory created in the current Polygon class
*/
Polygon::~Polygon()
{
	//nothing to delete here
}

