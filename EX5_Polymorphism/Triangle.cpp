#include "Triangle.h"


/*
The function constructs the current Triangle class according to the given elements
Input: 3 Points of the 3 sides of the triangle, the type (Triangle) and the name
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) :
	Polygon(type, name), _a(a), _b(b), _c(c)
{
	if (_a.getX() == _b.getX() && _a.getX() == _c.getX() || _a.getY() == _b.getY() && _a.getY() == _c.getY())  //checks if the x or y is the same in all the Triangle points
	{
		throw "The points entered create a line";
	}
	else
	{
		this->updateVector();
	}
}


/*
The function diconstructs the allocated dynamic memory created for the creation of the current Triangle class
*/
Triangle::~Triangle()
{
	//nothing to delete here
}


/*
The function draws the current Triangle class on the given canvas
*/
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}


/*
The function clears the current Triangle class from the given canvas
*/
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}


/*
The function calculates the area of the current Triangle class according to the formula:  sqrt(p * (p - AB)*(p - AC)*(p - BC)). p = perimeter / 2. 
Ouput: the area of the current triangle
*/
double Triangle::getArea() const
{
	//calculates the three sides of the current triangle 
	double lineAB = _a.distance(_b);  
	double lineBC = _b.distance(_c);
	double lineCA = _c.distance(_a);

	double p = this->getPerimeter() / 2;  
	double line1 = p - lineAB;
	double line2 = p - lineBC;
	double line3 = p - lineCA;

	return sqrt(p * line1 * line2 * line3);
}


/*
The function claculates the permieter of current Triangle class.
Output: the perimeter of the current triangle
*/
double Triangle::getPerimeter() const
{
	//calculates the three sides of the current triangle
	double lineAB = _a.distance(_b);
	double lineBC = _b.distance(_c);
	double lineCA = _c.distance(_a);

	return lineAB + lineBC + lineCA;  //returns the sum of the sides - perimeter
}


/*
The functions moves each point of the current Triangle class according to the given point
Output: a Point class which being used to move every point in the current triangle
*/
void Triangle::move(const Point& other)
{
	_a += other;
	_b += other;
	_c += other;

	this->updateVector();
}


/*
The function pushes each point of the current triangle to the vector to save them
*/
void Triangle::updateVector()
{
	_points.clear();

	_points.push_back(_a);
	_points.push_back(_b);
	_points.push_back(_c);
}

