#include "Menu.h"


/*
The funciton constructs the Menu class
*/
Menu::Menu()
{
	//creats the array with the pointer to the init shape functions
	_initShape[0] = &Menu::initCircle;
	_initShape[1] = &Menu::initArrow;
	_initShape[2] = &Menu::initTriangle;
	_initShape[3] = &Menu::initRectangle;

	//creats the array with the pointer to the edit shape functions
	_editShape[0] = &Menu::moveShape;
	_editShape[1] = &Menu::getDetails;
	_editShape[2] = &Menu::removeShape;
}	


/*
The function deconstructs the allocated dynamic memory allcoated for the creation of the shapes
*/
Menu::~Menu()
{
	this->clearVectors();
}


/*
This is the main menu. it keeps running until the user enters the value 3 - exit the loop and end the program
*/
void Menu::mainMenu()
{
	int option = 0;

	while (option != 3)  //runs until user enter 3 - exit
	{
		option = getMainOption();

		try
		{
			switch (option)
			{
			case 0:  //first option - choose a shape to add and draw
				chooseShapeOption();
				break;

			case 1:  //second opiton - choose a shape to edit or get information about
				if (_shapes.empty())  //checks if there are any shapes
				{
					throw "No shapes added.";
				}
				else
				{
					editShapeOption();
				}
				break;

			case 2:  //third option - deletes all shapes
				clearVectors();
				break;

			case 3:  //fourth option - exit the program
				std::cout << "GOOD BYE!" << std::endl;
				break;
			}
		}
		catch (const char* msg)
		{
			std::cout << msg << std::endl;
			system("pause");
		}
	}
}


/*
The function checks if the given number is between 0 and 3
Input: the number to check
*/
void Menu::checkInput(int& option, const int numOfOptions) const
{
	while ((std::cin.fail()) || (option < 0 || option > numOfOptions))  //checks if the number isnt between 0 and 3 and if the program recieved a defferent input than int
	{
		//ignores the wrong input
		std::cin.clear();
		std::cin.ignore();
		
		//gets a new input and checks if its between 0 and 3
		std::cout << "Wrong input! Please enter a number between 0 and 3: ";
		std::cin >> option;
	}
}


/*
The function prints the main menu to the user and gets his wanted opition
Output: the user's option
*/
int Menu::getMainOption()
{ 
	int option = 0;

	system("cls");  //clears the screen
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
	std::cin >> option;

	checkInput(option);  //checks if input is between 0 and 3 - according to number of options

	system("cls");  //clears the screen
	return option;
}


/*
The function prints the menu of choosing a shape, gets the user's option
*/
void Menu::chooseShapeOption()
{
	Shape* newShape;
	int option = 0;

	system("cls");  //clears the screen
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
	std::cin >> option;

	checkInput(option);  //checks if input is between 0 and 3 - according to number of options

	newShape = (this->*_initShape[option])();
	pushShape(newShape);

	system("cls");  //clears the screen
}


/*
The function prints the option of editing an existing shape and gets the option from the user
*/
void Menu::editShapeOption()
{
	int option = 0;
	int element = 0;

	element = printAll();
	checkInput(element, _shapes.size() - 1);  //checks if input is between 0 and the number of available shapes

	system("cls");  //clears the screen
	std::cout << "Enter 0 to move the shape." << std::endl;
	std::cout << "Enter 1 to get its details." << std::endl;
	std::cout << "Enter 2 to remove the shape." << std::endl;
	std::cin >> option;

	checkInput(option, 2);  //checks if input is between 0 and 2 - according to number of options

	//activates the wanted option with on the wanted element using the pointers to functions array
	(this->*_editShape[option])(element);
}


/*
The functions creates a point class according to the user's input
Ouput: a pointer to the creaatd point
*/
Point* Menu::createPoint()
{
	Point* p1;
	int x = 0;
	int y = 0;

	std::cout << "Enter X: ";
	std::cin >> x;
	std::cout << "Enter Y: ";
	std::cin >> y;
	p1 = new Point(x, y);

	return p1;
}


/*
The function creates the circle according to the users input and draws it on the canvas
Ouput: the cirlce pointer
*/
Shape* Menu::initCircle()
{
	Circle* newCircle;
	Point* p1;
	int radius = 0;
	std::string name;

	std::cout << "Enter the details for the center of the circle: " << std::endl;
	p1 = createPoint();

	std::cout << "Enter radius: ";
	std::cin >> radius;

	std::cin.get();  //cleans the buffer
	std::cout << "Enter name: ";
	std::getline(std::cin, name);

	newCircle = new Circle(*p1, radius, "Circle", name);
	newCircle->draw(_canvas);

	delete p1;

	return newCircle;
}


/*
The function creates the arrow according to the users input and draws it on the canvas
Ouput: the arrow pointer
*/
Shape* Menu::initArrow()
{
	Arrow* newArrow;
	Point* p1;
	Point* p2;
	std::string name;

	std::cout << "Enter the details for the first point: " << std::endl;
	p1 = createPoint();
	std::cout << "Enter the details for the second point: " << std::endl;
	p2 = createPoint();

	std::cin.get();  //cleans the buffer
	std::cout << "Enter name: ";
	std::getline(std::cin, name);

	newArrow = new Arrow(*p1, *p2, "Arrow", name);
	newArrow->draw(_canvas);

	delete p1;
	delete p2;

	return newArrow;
}


/*
The function creates the triangle according to the users input and draws it on the canvas
Ouput: the triangle pointer
*/
Shape* Menu::initTriangle()
{
	Triangle* newTriangle;
	Point* p1;
	Point* p2;
	Point* p3;
	std::string name;

	std::cout << "Enter the details for the first point: " << std::endl;
	p1 = createPoint();
	std::cout << "Enter the details for the second point: " << std::endl;
	p2 = createPoint();
	std::cout << "Enter the details for the second point: " << std::endl;
	p3 = createPoint();

	std::cin.get();  //cleans the buffer
	std::cout << "Enter name: ";
	std::getline(std::cin, name);

	newTriangle = new Triangle(*p1, *p2, *p3, "Triangle", name);
	newTriangle->draw(_canvas);

	delete p1;
	delete p2;
	delete p3;

	return newTriangle;
}


/*
The function creates the rectangle according to the users input and draws it on the canvas
Ouput: the rectangle pointer
*/
Shape* Menu::initRectangle()
{
	myShapes::Rectangle* newRectangle;
	Point* p1;
	int length = 0;
	int width = 0;
	std::string name;

	std::cout << "Enter the details for the top left point: " << std::endl;
	p1 = createPoint();

	std::cout << "Enter length: ";
	std::cin >> length;

	std::cout << "Enter width: ";
	std::cin >> width;

	std::cin.get();  //cleans the buffer
	std::cout << "Enter name: ";
	std::getline(std::cin, name);

	newRectangle = new myShapes::Rectangle(*p1, length, width, "Rectangle", name);
	newRectangle->draw(_canvas);

	delete p1;

	return newRectangle;
}


/*
The function pushes the given shape to the vectors
Input: the shape to save in the vector
*/
void Menu::pushShape(Shape* newShape)
{
	this->_shapes.push_back(newShape);  //pushes the shape to another vector
}


/*
The function prints all the created shapes from the vector and takes the user's option of what he want to edit
Output: the shape the user want to edit
*/
int Menu::printAll()
{
	int numOfShapes = _shapes.size();
	int i = 0;
	int option = 0;

	for (i = 0; i < numOfShapes; i++)
	{
		std::cout << "Enter " << i << " for the shape: ";
		_shapes[i]->printDetails();
	}
	std::cin >> option;


	return option;
}


/*
The funciton changes the point of the wanted shape, clears it from the canvas and draws it again
Input: the wanted shape element
*/
void Menu::moveShape(const int element)
{
	Point* p1;

	std::cout << "Enter a point to add to the shape: " << std::endl;
	p1 = createPoint();

	for (int i = 0; i < _shapes.size(); i++)  //clears the draw of all shapes
	{
		_shapes[i]->clearDraw(_canvas);
	}

	_shapes[element]->move(*p1);

	for (int i = 0; i < _shapes.size(); i++)  //draws again all of the shapes
	{
		_shapes[i]->draw(_canvas);
	}

	delete p1;
}


/*
The function prints the details of the wanted shape- name, type, area and perimeter
Input: the wanted shape element
*/
void Menu::getDetails(const int element)
{
	_shapes[element]->printDetails();
	std::cout << _shapes[element]->getArea() << ", " << _shapes[element]->getPerimeter() << std::endl;
	system("pause");
}


/*
The functions removes the shape from the vector and clears its drawing from the board
Input: the wanted shape element
*/
void Menu::removeShape(const int element)
{
	for (int i = 0; i < _shapes.size(); i++)  //clears the draw of all shapes
	{
		_shapes[i]->clearDraw(_canvas);
	}

	//deltes the wanted element
	delete _shapes[element];  
	_shapes.erase(_shapes.begin() + element);

	for (int i = 0; i < _shapes.size(); i++)  //draws again all of the shapes
	{
		_shapes[i]->draw(_canvas);
	}
}


/*
The function clears all the dynamic memory allocated for the creation of the shapes
*/
void Menu::clearVectors()
{
	for (int i = 0; i < _shapes.size(); i++)  //runs on the vector
	{
		_shapes[i]->clearDraw(_canvas);  //clears drawing of the canvas
		delete _shapes[i];  //deletes the dynamic memory of the shape
	}

	//clears both of the bectors just incase
	_shapes.clear(); 
}

