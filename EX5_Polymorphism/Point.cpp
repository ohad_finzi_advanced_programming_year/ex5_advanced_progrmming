#include "Point.h"


/*
The function constructs the current Point class according to the given parameters
Input: x and y for the point
*/
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

/*
The function constructs the current Point class according to the given parameters
Input: a point to copy its x and y values to the current Point class
*/
Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}


/*
The function deconstruct the current Point class by deleting all the dynamic memory created for the class
*/
Point::~Point()
{
	//nothing to delete here
}


/*
The function overloads the operator '+'
Input: a Point class
Output: a new Point class which its values are the sum of the current Point class and the given Point class
*/
Point Point::operator+(const Point& other) const
{
	return Point(this->_x + other.getX(), this->_y + other.getY());
}


/*
The function overloads the operator '+'
Input: a Point class
Output: the current Point class but the values of the given Point class were added to the current Point class
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y += other.getY();

	return *this;
}


/*
The functios returns the current Point class X value
*/
double Point::getX() const
{
	return this->_x;
}


/*
The functios returns the current Point class Y value
*/
double Point::getY() const
{
	return this->_y;
}


/*
The function calculates the distance between the current Point class and the given Point class
Input: a Point class
Output: the distance between the current Point class and the given Point class
*/
double Point::distance(const Point& other) const
{
	double calcX = (this->_x - other.getX()) * (this->_x - other.getX());
	double calcY = (this->_y - other.getY()) * (this->_y - other.getY());
	return sqrt(calcX + calcY);
}

