#pragma once

#include "Shape.h"
#include "Canvas.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"
#include <vector>


#define NUM_OF_SHAPES 4
#define NUM_OF_OPTIONS 3
#define NUM_OF_EDIT_FUNCTIONS 3


class Menu
{
public:

	Menu();
	~Menu();

	void mainMenu();

	//user input option related functions:
	void checkInput(int& option, const int numOfOptions = NUM_OF_OPTIONS) const;
	int getMainOption();
	void chooseShapeOption();
	void editShapeOption();

	Point* createPoint();

	//initalizing all the shapes
	Shape* initCircle();
	Shape* initArrow();
	Shape* initTriangle();
	Shape* initRectangle();
	void pushShape(Shape* newShape);

	//all the funcitons for the second menu (move shape, remove shape, delte shape)
	int printAll();
	void moveShape(const int element);
	void getDetails(const int element);
	void removeShape(const int element);

	void clearVectors();

private:
	Canvas _canvas;

	//The function array which controls on the shape initialization according to the user's input
	typedef Shape* (Menu::* initShapeFunciotns)();
	initShapeFunciotns _initShape[NUM_OF_SHAPES];

	//The function array which controls the edit funcitons of the shapes according to the user's input
	typedef void (Menu::* editShapeFunctions)(int element);
	editShapeFunctions _editShape[NUM_OF_EDIT_FUNCTIONS];

	std::vector<Shape*> _shapes;
};

