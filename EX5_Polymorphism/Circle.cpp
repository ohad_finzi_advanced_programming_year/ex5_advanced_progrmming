#include "Circle.h"


/*
The function constructs the current Circle class accoring to the given parameters
Input: a Point class, radius of the cirlce, type (Circle) and name of the object
*/
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name):
	Shape(type, name), _center(center), _radius(radius)
{
	if (this->_radius < 0)
	{
		throw "Can't create a circle with negative radius";
	} 
}


/*
The function deconstruct the current Circle class by deleting all the dynamic memory created for the class
*/
Circle::~Circle()
{
	//nothing to delete here
}


/*
The function returns the center Point class of the current Cirlce class
Output: the center Point class of the current Cirlce class
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}


/*
The function returns the radius of the current Cirlce class
Output: the radius of the current Cirlce class
*/
double Circle::getRadius() const
{
	return this->_radius;
}


/*
The function draws the current Circle class on the given canvas according to the current Class data
*/
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}


/*
The function clears the current Circle class of the given canvas according to the current Class data
*/
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


/*
The function calculates the area of the current Circle class according to its elemetns
Output: the area of the current Circle class according to its elemetns
*/
double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}


/*
The function calculates the perimeter of the current Circle class according to its elemetns
Output: the perimeter of the current Circle class according to its elemetns
*/
double Circle::getPerimeter() const
{
	return 2 * this->_radius * PI;
}


/*
The function clears the board and draws a new circle according to the given Point class
Input: a Point class 
*/
void Circle::move(const Point& other)
{
	this->_center += other;
}

