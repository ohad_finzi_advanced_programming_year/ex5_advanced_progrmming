#include "Shape.h"


/*
The function constructs the current Shape class according to the given elements
Input: name of the class and type
*/
Shape::Shape(const std::string& type, const std::string& name)
{
	this->_name = name;
	this->_type = type;
}


/*
The functions diconstructs the dynamic memory allocated for the creation of the current Shape class
*/
Shape::~Shape()
{
	//nothing to delete here
}


/*
The function prints the details of the current Shape class: name, type, area and perimeter
*/
void Shape::printDetails() const
{
	std::cout << this->_name << "( " << this->_type << " )" << std::endl;
}


/*
The functions returns the type of the current Shape class
Output: the current Shape class type
*/
std::string Shape::getType() const
{
	return this->_type;
}


/*
The functions returns the name of the current Shape class
Output: the current Shape class name
*/
std::string Shape::getName() const
{
	return this->_name;
}

