#include "Rectangle.h"


/*
The function constructs the current Rectangle class according to the given elements
Input: the top left point of the Rectangle, the width and the length of the Rectangle and the type (Rectangle) and name of object
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name):
	Polygon(type, name), _topLeft(a), _bottomRight(a.getX() + length, a.getY() - width)  //initializng the Polygon class, the top left point and the bottom right point  (x = x + length, y = y - width)
{
	if (length < 0 || width < 0)
	{
		throw "Rectangle sides can't be below 0";
	}
	else
	{
		this->updateVector();
	}
}


/*
The function diconstructs the allocated dynamic memory created in the current Rectangle class
*/
myShapes::Rectangle::~Rectangle()
{
	//nothing to delete here
}


/*
The function draws the current Rectangle on the given canvas
*/
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}


/*
The function clears  the current Rectangle draw from the given canvas
*/
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


/*
The function calculates the area of the current Recatangle by calculating its width and length using the two points
*/
double myShapes::Rectangle::getArea() const
{
	int length = _bottomRight.getX() - _topLeft.getX();  //length = bottom right x - top left x
	int width = _topLeft.getY() - _bottomRight.getY();  //length = top left y - bottom right y 

	return (length * width);
}


/*
The function calculates the perimeter of the current Recatangle by calculating its width and length using the two points
*/
double myShapes::Rectangle::getPerimeter() const
{
	int length = _bottomRight.getX() - _topLeft.getX();  //length = bottom right x - top left x
	int width = _topLeft.getY() - _bottomRight.getY();  //length = top left y - bottom right y 

	return (width + width + length + length);
}


/*
The function moves each point of the current Rectangle class according to the given point
Output: a Point class which being used to move every point in the current rectangle
*/
void myShapes::Rectangle::move(const Point& other)
{
	_topLeft += other;
	_bottomRight += other;

	this->updateVector();
}


/*
The function pushes each point of the current triangle to the vector to save them
*/
void myShapes::Rectangle::updateVector()
{
	_points.clear();

	_points.push_back(_topLeft);
	_points.push_back(_bottomRight);
}

